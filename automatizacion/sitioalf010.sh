#!/bin/bash

# Verificando que apache este instalado o instalando
if rpm -q httpd > /dev/null 2>&1
then
        echo "verificando si httpd esta instalado -----> OK"
else
        echo "verificando si httpd esta instalado -----> NOK"
        echo "instalando httpd..."
        yum install httpd -y > /dev/null 2>&1
        if rpm -q httpd > /dev/null 2>&1
        then
                echo "verificando si httpd esta instalado -----> OK"
        else
                echo "verificando si httpd esta instalado -----> NOK"
                echo "El script no puede continuar ...."
                exit 1
        fi
fi


# Iniciando Apache
if systemctl status httpd > /dev/null 2>&1
then
        echo "verificando si httpd esta ejecutandose --> OK"
else
        echo "verificando si httpd esta ejecutandose --> NOK"
        echo "iniciando httpd..."
        if systemctl start httpd > /dev/null 2>&1
        then
                echo "verificando si httpd esta ejecutandose --> OK"
        else
                echo "verificando si httpd esta ejecutandose --> NOK"
                exit 2
        fi
fi

# Verificando si apache esta activado para inicio con el sistema
if systemctl is-enabled httpd > /dev/null 2>&1
then
        echo "verificando si httpd esta habilitado ----> OK"
else
        echo "verificando si httpd esta habilitado ----> NOK"
        echo "habilitado httpd..."
        if systemctl enable httpd > /dev/null 2>&1
        then
                echo "verificando si httpd se ha habilitado ---> OK"
        else
                echo "verificando si httpd se ha habilitado ---> NOK"
                exit 3
        fi
fi

# Verificando si apache tiene el puerto 80 andando
var1=`ss -tapnl | grep 'users:(("httpd",' | grep \:80 | wc -l`
if [ ${var1} == 1 ]
then
        echo "verificando si httpd en puerto 80 andando> OK"
else
        echo "verificando si httpd en puerto 80 andando> NOK"
        echo "Revise... algo salio muy mal"
        exit 4
fi

# Verificando Firewall
if systemctl status firewalld > /dev/null 2>&1
then
        echo "verificando si FW esta ejecutandose -----> OK"
else
        echo "verificando si FW esta ejecutandose -----> NOK"
        echo "iniciando firewalld..."
        if systemctl start firewalld > /dev/null 2>&1
        then
                echo "verificando si FW esta ejecutandose -----> OK"
        else
                echo "verificando si FW esta ejecutandose -----> NOK"
                exit 5
        fi
fi

# Verificando si apache esta activado para inicio con el sistema
if systemctl is-enabled firewalld > /dev/null 2>&1
then
        echo "verificando si FW esta habilitado -------> OK"
else
        echo "verificando si FW esta habilitado -------> NOK"
        echo "habilitado firewalld..."
        if systemctl enable firewalld > /dev/null 2>&1
        then
                echo "verificando si FW se ha habilitado ------> OK"
        else
                echo "verificando si FW se ha habilitado ------> NOK"
                exit 6
        fi
fi

firewall-cmd --add-port=80/tcp  --permanent > /dev/null 2>&1
firewall-cmd --reload > /dev/null 2>&1
echo "Habilitando puerto 80 -------------------> OK"

# Verificar que GIT esta instalado
if rpm -q git > /dev/null 2>&1
then
        echo "verificando si git esta instalado -------> OK"
else
        echo "verificando si git esta instalado -------> NOK"
        echo "instalando git..."
        yum install git -y > /dev/null 2>&1
        if rpm -q git > /dev/null 2>&1
        then
                echo "verificando si git esta instalado -------> OK"
        else
                echo "verificando si git esta instalado -------> NOK"
                echo "El script no puede continuar ...."
                exit 1
        fi
fi

# Verificar que existen las llaves

if [ -d "/root/.ssh" ]
then
        echo "verificando si dir .ssh existe ----------> OK"
else
        echo "Creando directorio /root/.ssh"
        if mkdir -m 700 /root/.ssh
        then
                echo "Directorio /root/.ssh creado ------------> OK"
        else
                echo "Directorio /root/.ssh creado ------------> NOK"
                exit 1
        fi
fi

# LLAVES BitBucket
echo "-----BEGIN RSA PRIVATE KEY-----" > /root/.ssh/bitbucket2
echo "MIIEogIBAAKCAQEA1mKwVzGwhBX1H2eU7XzwIcrntnfhi5K+YcZc5W7WgunRPWqe" >> /root/.ssh/bitbucket2
echo "FIrDuB6zyLLMeg20v++zcVZeHdCQv5V+Y7mhhJmgT4A6LAsE0IoxQ7cqPZdH4b3H" >> /root/.ssh/bitbucket2
echo "QgJLnqXvMIi38Yfz4zwfqzk8RHoZzQha2SYjoeHdZFloO0dTc8kYdFFD63FwmNBz" >> /root/.ssh/bitbucket2
echo "TtFp2bn8eR70Ovs9lvqBSPNx627cWg51DLDEeAddxElqKqJKOWJiUiIS6BXf3d8E" >> /root/.ssh/bitbucket2
echo "E9bNdk98FE/aFEwZEESHiRhPfTOXEAVuJgIbmU+rPpH8G1IQcd4ZhK+DFFZaw0r5" >> /root/.ssh/bitbucket2
echo "GctpJVl/oR3mie/al9RcTX/QyTTDlUIgwVP1SwIDAQABAoIBAE1NUoZuPVazUCqd" >> /root/.ssh/bitbucket2
echo "v96OUUvuDPUHbwVTyWAaFX8JI0IbKz55Bj0j+ASY1XzFs+NCs+0Ncmeom9EmVjsu" >> /root/.ssh/bitbucket2
echo "nV86HRpiBqMD+nXNF1RWsALZtdp1hQ7JzmJnz3qKHKAkfCcUPYWvNUAZKDkMyebb" >> /root/.ssh/bitbucket2
echo "X16zWN/TQHAhqOrPAjFazn2Km/H10W3V3/kQtJqTMsTuUQDhuyMEpeO29fR2MR7X" >> /root/.ssh/bitbucket2
echo "I2NbdT+YqOVynxgZStDuURooaMf6+zTMGya/5++fr1cghTzeCyFlKzoCm1bM8a75" >> /root/.ssh/bitbucket2
echo "QhQyotkQGc659usL4xnBIDe5iPA82Io4oBAdEEitgPDkIvXxArcp6ZLJGhScdBN9" >> /root/.ssh/bitbucket2
echo "mSk82XECgYEA89Yh7N1LAiJdhI57zYFnfQAxXvZkl0OB1oZyxcyEEm4qDaeLXiCM" >> /root/.ssh/bitbucket2
echo "hkNw96QCb0BM4axG37qo3ke0ro/vrBjfKjzXxNZ1gl99xOD3m5+7DoGBRQZLTNe1" >> /root/.ssh/bitbucket2
echo "r5durteKHKXoxVlyGN/1oKSA+P589PrkrUOAb2prr5alQ7l9UKXiIrMCgYEA4RR1" >> /root/.ssh/bitbucket2
echo "XwLbM01T5UbONAalyherwV88CaVbV5AdROgGurGV5Rx/mTMKkJkURFCHoMqNMQaP" >> /root/.ssh/bitbucket2
echo "tBXOlWmFF+kw9CDlBO6GviKT1jYoY1QKDUSnJvdL2Xcv5/ycq1Mi94gWmwd15+3v" >> /root/.ssh/bitbucket2
echo "V0ZsA5y41P+hkAxMOTYpTKfxjSzmWlJq90VmzwkCgYAkcXWilDR4vjAh9rW0MkoI" >> /root/.ssh/bitbucket2
echo "WbYHTmYcYX+PU4rl2zpGEucAIXFoAQ9fpcbFjGK8w+x5sijpmVlR13++s06L9y+r" >> /root/.ssh/bitbucket2
echo "fWp2ReNMN/9xJHp9bPMlKN/sNBOfCYJjGwv038wJmhZfII43/4kTmGKaVFbSuojh" >> /root/.ssh/bitbucket2
echo "CHyyqkIr/ST4KUn8er6AlwKBgAvHrFTqq1pGA8wOJqCmcGrhTkXynAvNMbilfEKs" >> /root/.ssh/bitbucket2
echo "+h0vSi8RjMQR3c2vlVX1+QM7VqRoOYEtWwCK40JbHArK5fGaIkc1PBOMBhUcwuHq" >> /root/.ssh/bitbucket2
echo "4+ywKLIbANDdcb2tvjjCUJyQiWU4GLyynNibjTjooCYP3rVn1kWCzKuc2/mCMsL6" >> /root/.ssh/bitbucket2
echo "SOUxAoGAfHoRYcBEwoa717TXnZ6nmtWBCav+UM5dbyD2fynfZF35AoFs0xhlKFmS" >> /root/.ssh/bitbucket2
echo "W/xN5Rj2kQm2L05cjqbQIjXoI5v0OYj8NAIrSsL17kYkOOB9oE3pIoEdMdj739hk" >> /root/.ssh/bitbucket2
echo "44GM0L4n46rpO3p4pFFyIPnobYATnT6uGVfwShgpDNcGPRTf2xY=" >> /root/.ssh/bitbucket2
echo "-----END RSA PRIVATE KEY-----" >> /root/.ssh/bitbucket2
chmod 600 /root/.ssh/bitbucket2

if [ -f "/root/.ssh/bitbucket2" ]
then
        echo "Archivo /root/.ssh/bitbucket2 existe ----> OK"
else
        echo "Archivo /root/.ssh/bitbucket2 existe ----> NOK"
fi

# Agregar llave de bitbucket como identidad de SSH
eval `ssh-agent -s` > /dev/null 2>&1 
echo "Se ejecuta ssh-agent -s -----------------> OK"
if ssh-add /root/.ssh/bitbucket2 > /dev/null 2>&1
then
	echo "Identidad /root/.ssh/bitbucket2 agregada > OK"
else
	echo "Identidad /root/.ssh/bitbucket2 agregada > NOK"
	exit 1
fi

#Clonar repositorio
if rm -rf /var/www/html/* > /dev/null 2>&1
then
	echo "Limpiando archivos en directorio html ---> OK"
else
	echo "Limpiando archivos en directorio html ---> NOK"
fi


if rm -rf /var/www/html/.* > /dev/null 2>&1
then
	echo "Limpiando archivos en directorio html ---> OK"
else
	echo "Limpiando archivos en directorio html ---> NOK"
fi

{
	ssh-keyscan -H bitbucket.org > /root/.ssh/known_hosts
} &> /dev/null 2>&1

if [ "$?" == 0 ]
then
	echo "Agregando llave de bitbucket al sistema -> OK"
else
	echo "Agregando llave de bitbucket al sistema -> NOK"
fi

( cd /var/www/html && git clone git@bitbucket.org:chinosanchezo/sitioweb-alf010.git ./  > /dev/null 2>&1 )
if [ "$?" == 0 ]
then
        echo "Clonando proyecto bitbucket -------------> OK"
else
        echo "Clonando proyecto bitbucket -------------> NOK"
fi